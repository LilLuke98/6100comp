import React from 'react';
import { View, StyleSheet, ScrollView, Image } from 'react-native';

import Screen from "../components/Screen";
import AppText from "../components/AppText";
import AppButton from "../components/AppButton";
import routes from '../navigation/routes';
import AccountNavigator from "../navigation/AccountNavigator";

function ContactUs({ navigation }) {
    return (
        <ScrollView style={styles.Screen}>
        <Screen>
            <View>
                <AppText>Welcome to Trusted Mechanics and thank you for trusting us to find you the best price possible! Here at Trusted Mechanics we make it our mission to ensure the POWER of overcharging for maintenance is gone and the pwoer is now in your hands! Want to know how it works? Read further and we'll help you never be overcharged again!.</AppText>
                <View style={styles.imageContainer}>
                <Image style={styles.Image} source={require("../assets/LukeRhodes.png")} />
                </View>
                <AppText>Using our site you are given the power to affect the price you pay for maintenance to your vehicle! If you have an issue with your vehicle you can post a listing with your issue and what you need fixing, set a price you want this issue to be fixed for and then other users can use the app and see your post! upon viewing your post people can message you and make offers on how much this would be to fix for them. There is NO PRESSURE to accept any offer if you feel it is over priced and therefore users get the best price offered right to them!</AppText>    
                <AppText style={styles.Text}>Give it a go its FREE to create an account and start saving...</AppText>
                <AppText style={styles.subHeading}>NOW!</AppText>
                <AppText> Email: rhodes.luke@ymail.com</AppText>
                <AppText> Phone: 07711078423</AppText>
                <AppText> Post: 6 Geneva Road, Liverpool, L6 3AS</AppText>
            </View>
        </Screen>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    imageContainer: {
        alignItems: 'center',
    },
    Image: {
        width: 200,
        height: 200,
    },
    subHeading: {
        fontSize: 25,
        fontWeight: "600",
        paddingVertical: 5,
        textAlign: 'center',
    },
    Screen: {
        backgroundColor: '#6be4f8' 
    },
    Text: {
        paddingVertical: 20,
        textAlign: 'center',
    }
})

export default ContactUs;