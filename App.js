import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import  AppLoading  from 'expo-app-loading';

//import WelcomeScreen from "./app/screens/WelcomeScreen";
//import AppText from './app/components/AppText';
//import AppButton from './app/components/AppButton';
//import ViewImageScreen from './app/screens/ViewImageScreen';
//import MessagesScreen from './app/screens/MessagesScreen';
//import ListingDetailsScreen from "./app/screens/ListingDetailsScreen";
//import AccountScreen from "./app/screens/AccountScreen";
//import ListingsScreen from "./app/screens/ListingsScreen";
//import Screen from './app/components/Screen';
//import AppTextInput from './app/components/AppTextInput';
//import AppPicker from "./app/components/AppPicker";
//import LoginScreen from "./app/screens/LoginScreen";
//import RegisterScreen from "./app/screens/RegisterScreen";
//import ListingEditScreen from "./app/screens/ListingEditScreen";
//import Card from "./app/components/Card";
//import ImageInput from "./app/components/ImageInput";
//import ImageInputList from "./app/components/ImageInputList";
import AuthNavigator from './app/navigation/AuthNavigator';
import navigationTheme from './app/navigation/navigationTheme';
import AppNavigator from './app/navigation/AppNavigator';
import OfflineNotice from './app/components/OfflineNotice';
import AuthContext from './app/auth/context';
import authStorage from './app/auth/storage';
//import ContactUs from "./app/screens/ContactUs";
import  navigationRef  from './app/navigation/rootNavigation';

export default function App() {
  const [user, setUser] = useState();
  const [isReady, setIsReady] = useState(false);

  const restoreUser = async () => {
    const user = await authStorage.getUser();
    if (user) setUser(user);
  };

  if (!isReady)
    return (
      <AppLoading startAsync={restoreUser} onFinish={() => setIsReady(true)} onError={() => setIsReady(true)} />
    );

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <OfflineNotice />
      <NavigationContainer ref={navigationRef} theme={navigationTheme}>
        {user ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
  //return <ContactUs />
}
